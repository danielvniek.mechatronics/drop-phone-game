﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AddGenerator : MonoBehaviour
{
    public static AddGenerator instance;
    private string appID = "ca-app-pub-7646068694365624~2139228358";

    private BannerView bannerView;
    private string bannerID = "ca-app-pub-7646068694365624/5143492180";

    private RewardBasedVideoAd vidAd;
    private string vidAdID = "ca-app-pub-7646068694365624/5932354891";
    public static int vidChoice = 1;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    private void Start()
    {

        MobileAds.Initialize(appID);
        AddGenerator.instance.RequestBanner();
        vidAd = RewardBasedVideoAd.Instance;
        RequestVidAd();

        vidAd.OnAdLoaded += HandleRewardBasedVideoLoaded;
        vidAd.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        vidAd.OnAdRewarded += HandleRewardBasedVideoRewarded;
        vidAd.OnAdClosed += HandleRewardBasedVideoClosed;
        

        
    }
    public void RequestBanner()
    {
        bannerView = new BannerView(bannerID, AdSize.Banner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        bannerView.LoadAd(request);
        bannerView.Show();
    }
    public void RequestVidAd()
    {
        
        AdRequest request = new AdRequest.Builder().Build();
        vidAd.LoadAd(request,vidAdID);
    }
    public void ShowVidAdd()
    {
        if (vidAd.IsLoaded())
        { 
            Debug.Log("vid");
            vidAd.Show();
        }
        else
        {
            Debug.Log("vid not loaded");
        }
    }

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        Debug.Log("Vid ad loaded successfully");
    }
    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("Failed to load rewarded video ad : " + args.Message);
        //RequestVidAd();
    }
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
       if(vidChoice == 2)
        {
            
            Health.healthMax = Health.healthMax + 0.2f;
            if (Health.healthMax > 8)
            {
                Health.healthMax = 8;
            }
            PlayerPrefs.SetFloat("healthMax", Health.healthMax);
            Health.currentHealth = Health.healthMax;
        }
        else
        {
            Attempts.nrAttempts = Attempts.nrAttempts + 50;
            PlayerPrefs.SetInt("attempts", Attempts.nrAttempts);
        }
        
  

    }
    
    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        Debug.Log("Rewarded video has closed");
        RequestVidAd();
    }
}

