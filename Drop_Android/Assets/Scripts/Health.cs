﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;

public class Health : MonoBehaviour
{

    public static float healthT = 0f;
    
    public GameObject healthBar;
    public static float healthMax = 3f;
    public static float currentHealth = healthMax;
    public static bool startDying = false;
    public GameObject barColour;
 



    // Start is called before the first frame update
    void Start()
    {
        healthMax = PlayerPrefs.GetFloat("healthMax");
        healthBar.GetComponent<Transform>().localScale = new Vector3(healthMax, 1, 1);
        currentHealth = healthMax;
        healthBar.GetComponent<Transform>().localScale = new Vector3(currentHealth, 1, 1);


    }

    // Update is called once per frame
    void Update()
    {
           if (currentHealth > 6f)
        {
            barColour.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else if(currentHealth >2f)
        {
            barColour.GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        else
        {
            barColour.GetComponent<SpriteRenderer>().color = Color.red;
        }

        healthBar.GetComponent<Transform>().localScale = new Vector3(currentHealth, 1, 1);
        if ((BucketGenerator.start == true)&&(startDying == true))
        {
            
            healthT = healthT + Time.deltaTime;
            currentHealth = (2*healthMax - healthT)/2;
       
            if (currentHealth <= 0)
            {
                if (BucketGenerator.highScore < BucketGenerator.score)
                {
                    BucketGenerator.highScore = BucketGenerator.score;
                    BucketGenerator.user.userScore = BucketGenerator.highScore;
                    RestClient.Put("https://drop-80410.firebaseio.com/" + BucketGenerator.userId + ".json", BucketGenerator.user);


                }
                BucketGenerator.score = 0;
                BucketGenerator.start = false;

                for (int i = 0; i < BucketGenerator.allTheBuckets.Count; i++)
                {
                    Destroy(BucketGenerator.allTheBuckets[i]);
                }
                BucketGenerator.allTheBuckets.Clear();
                for (int i = 0; i < BallGenerator.allTheBalls.Count; i++)
                {
                    Destroy(BallGenerator.allTheBalls[i]);
                }
                BallGenerator.allTheBalls.Clear();
                currentHealth = healthMax;
                
                healthT = 0;
                startDying = false;
                Attempts.nrAttempts--;
                PlayerPrefs.SetInt("attempts", Attempts.nrAttempts);
            }
        }
        
        

    }
    public void IncreaseMaxHealth()
    {
        AddGenerator.vidChoice = 2;
        AddGenerator.instance.ShowVidAdd();
    }
}
