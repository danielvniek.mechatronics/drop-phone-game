﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;

public class CollisionScript : MonoBehaviour
{
    public AudioSource impact;
    private void Start()
    {
        impact = GetComponent<AudioSource>();

    }



    void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.GetComponent<SpriteRenderer>().color == GetComponent<SpriteRenderer>().color)
        {
            if (col.gameObject.GetComponent<Rigidbody2D>().position.y < 1)
            { 
            Health.startDying = true;
            Health.healthT = Health.healthT - 1;
            if (Health.healthT < 0)
            {
                Health.healthT = 0;
            }

            impact.Play();
            BucketGenerator.score++;
            }

        }
        else
        {
            
            if (BucketGenerator.highScore < BucketGenerator.score)
            {
                BucketGenerator.highScore = BucketGenerator.score;
                BucketGenerator.user.userScore = BucketGenerator.highScore;
                RestClient.Put("https://drop-80410.firebaseio.com/" + BucketGenerator.userId + ".json", BucketGenerator.user);


            }
            BucketGenerator.score = 0;
            BucketGenerator.start = false;
      
            for (int i = 0; i < BucketGenerator.allTheBuckets.Count; i++)
            {
                Destroy(BucketGenerator.allTheBuckets[i]);
            }
            BucketGenerator.allTheBuckets.Clear();
            for (int i = 0; i < BallGenerator.allTheBalls.Count; i++)
            {
                Destroy(BallGenerator.allTheBalls[i]);
            }
            BallGenerator.allTheBalls.Clear();
            Health.currentHealth = Health.healthMax;
           
            Health.healthT = 0;
            Health.startDying = false;
            Attempts.nrAttempts--;
            
            PlayerPrefs.SetInt("attempts", Attempts.nrAttempts);





        }
    }
}

