﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;

public class findTotalPlayers : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((startRanking.startRank == 1))
        {
            
            RestClient.Get<User>("https://drop-80410.firebaseio.com/" + "lastUser" + ".json").Then(response =>
            {
                startRanking.totalPlayers = response.userID;
                
            }
);
            startRanking.count = 1;
            startRanking.rank = 1;
            
            startRanking.startRank = 0;
        }

    }
}
