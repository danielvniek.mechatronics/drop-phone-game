﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Attempts : MonoBehaviour
{
    Text AttemptText;
    public static int nrAttempts = 5;
    // Start is called before the first frame update
    void Start()
    {
        nrAttempts = PlayerPrefs.GetInt("attempts");
        
        AttemptText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        AttemptText.text = "Attempts left: " + nrAttempts;
        
    }
    public void watchAttemptVid()
    {
        AddGenerator.vidChoice = 1;
        //Debug.Log("show");
        AddGenerator.instance.ShowVidAdd();
    }
}
