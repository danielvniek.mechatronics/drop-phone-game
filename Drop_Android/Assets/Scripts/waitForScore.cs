﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waitForScore : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(startRanking.playerscore >=0)
        {
            
            if (startRanking.playerscore > BucketGenerator.highScore)
            {
                startRanking.rank = startRanking.rank + 1;
    
                
            }
            
            startRanking.count = startRanking.count + 1;
            startRanking.playerscore = -1;
            if (startRanking.count == startRanking.totalPlayers)
            {
                BucketGenerator.ranking = startRanking.rank;
                startRanking.totalPlayers = 0;
                startRanking.startRank = 1;
                startRanking.playerscore = -1;
                startRanking.updateRank = 1;
            }
            
        }

        
    }
}
