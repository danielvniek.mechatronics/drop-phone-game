﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;
public class BucketGenerator : MonoBehaviour
{
    public GameObject bucketPrefab;
    private GameObject newBucket;
    private Rigidbody2D rb;
    float t = 0f;
    float tInterval = 1f;
    float bucketSpeed = 2f;
    float mintime = 1f;
    float maxtime = 3f;
    public static bool start = false;
    int bucketColor = 1;
    public static int score = 0;
    public static int ranking = 1;
    public static int highScore = 0;
    public static List<int> allTheScores = new List<int>();
    public static List<GameObject> allTheBuckets = new List<GameObject>();
    public static int validUser = 0;
    public static User user = new User();
    public static string userId;


   
    // Start is called before the first frame update

    void Start()
    {
        
        userId = PlayerPrefs.GetString("userId4");
        validUser = PlayerPrefs.GetInt("validUser4");
        


        RestClient.Get<User>("https://drop-80410.firebaseio.com/" + userId + ".json").Then(response =>
        {
            highScore = response.userScore;
        }
     );
        startRanking.startRank = 1;
    }
    

    // Update is called once per frame
    void Update()
    {
        if (start == true)
        {
            t = t + Time.deltaTime;
            if (t >= tInterval)
            {
                mintime = 4 / bucketSpeed;
                maxtime = mintime + 2;
                Vector2 spawnpos = new Vector2(11, -2);
                newBucket = Instantiate(bucketPrefab, spawnpos, Quaternion.identity);
                //newBucket.GetComponent<Rigidbody2D>();
                rb = newBucket.GetComponent<Rigidbody2D>();
                bucketColor = Random.Range(0, 30);
                if (bucketColor < 10)
                {
                    newBucket.GetComponent<SpriteRenderer>().color = Color.red;
                }
                else if (bucketColor <20)
                {
                    newBucket.GetComponent<SpriteRenderer>().color = Color.blue;
                }
                else{                
                    newBucket.GetComponent<SpriteRenderer>().color = Color.green;
                }
                rb.velocity = new Vector2(-1 * bucketSpeed, 0);
                tInterval = Random.Range(mintime, maxtime);
                if (bucketSpeed >= 8)
                {
                    bucketSpeed = 8;
                }
                else
                {
                    bucketSpeed = bucketSpeed + 0.05f;
                }
                //Debug.Log(bucketSpeed);
                t = 0;

            }
            //newBucket.GetComponent<Rigidbody2D>().velocity = new Vector2(-1 * bucketSpeed, 0);
            allTheBuckets.Add(newBucket);
        }
        else
        {
            start = false;
            bucketSpeed = 2f;
        }
 

    }
}
