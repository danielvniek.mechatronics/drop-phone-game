﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    public GameObject ballPrefab;
    private GameObject newBall;
    private Rigidbody2D rb;
    public static List<GameObject> allTheBalls = new List<GameObject>();

    public void RedBallGen()
    {
        Vector2 spawnpos = new Vector2(-3, 4);
        newBall = Instantiate(ballPrefab, spawnpos, Quaternion.identity);
        newBall.GetComponent<SpriteRenderer>().color = Color.red;
        rb = newBall.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, -1);
        allTheBalls.Add(newBall);
    }
    public void RlueBallGen()
    {
        Vector2 spawnpos = new Vector2(-3, 4);
        newBall = Instantiate(ballPrefab, spawnpos, Quaternion.identity);
        newBall.GetComponent<SpriteRenderer>().color = Color.blue;
        rb = newBall.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, -1);
        allTheBalls.Add(newBall);
    }
    public void GreenBallGen()
    {
        Vector3 spawnpos = new Vector3(-3, 4, 10);
        newBall = Instantiate(ballPrefab, spawnpos, Quaternion.identity);
        newBall.GetComponent<SpriteRenderer>().color = Color.green;
        rb = newBall.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0, -1);
        allTheBalls.Add(newBall);
    }







}
