﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class User
{
    public int userID;
    public int userScore;

    public User()
    {
        userID = 0;
        userScore = 0;
    }
}
