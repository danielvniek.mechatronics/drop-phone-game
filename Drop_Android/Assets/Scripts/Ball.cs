﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D rigidBody;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = new Vector2(0, -2);
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Rigidbody2D>().position.x <= -11)
        {
            Destroy(gameObject);

        }

    }

}
