﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;


public class PlayVisible : MonoBehaviour
{
    public GameObject PB;
    public GameObject GreenB;
    public GameObject BlueB;
    public GameObject RedB;
    public GameObject SubmitB;
    public GameObject UserName;
    public GameObject info;
    public GameObject attemptB;
    public GameObject healthB;
    public GameObject HowToB;
    public GameObject BackB;
    public int checkD=0;
    int information = 0;
   
    // Start is called before the first frame update
    void Start()
    {
        PB.SetActive(false);
        GreenB.SetActive(false);
        BlueB.SetActive(false);
        RedB.SetActive(false);
        info.SetActive(false);
        attemptB.SetActive(false);
        healthB.SetActive(false);
        HowToB.SetActive(false);
        BackB.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (BucketGenerator.validUser == 1)
        {
            if (checkD == 0)
            {
                PB.SetActive(true);
                HowToB.SetActive(false);
                attemptB.SetActive(true);
                healthB.SetActive(true);
                GreenB.SetActive(false);
                BlueB.SetActive(false);
                RedB.SetActive(false);
                SubmitB.SetActive(false);
                UserName.SetActive(false);
                checkD = 1;
            }
            if (information == 0)
            {
                if (BucketGenerator.start == true)
                {
                    PB.SetActive(false);
                    HowToB.SetActive(false);
                    attemptB.SetActive(false);
                    healthB.SetActive(false);
                    GreenB.SetActive(true);
                    BlueB.SetActive(true);
                    RedB.SetActive(true);
                }
                else
                {
                    PB.SetActive(true);
                    HowToB.SetActive(true);
                    attemptB.SetActive(true);
                    healthB.SetActive(true);
                    GreenB.SetActive(false);
                    BlueB.SetActive(false);
                    RedB.SetActive(false);

                }
            }
        }
 
        if ((BucketGenerator.user.userID != 0)&&(BucketGenerator.validUser == 0))
        {
            Debug.Log("in");
            BucketGenerator.validUser = 1;
            PlayerPrefs.SetInt("validUser4", BucketGenerator.validUser);
            RestClient.Put("https://drop-80410.firebaseio.com/" + "lastUser" + ".json", BucketGenerator.user);


        }
      
    }
    public void HowToPlay()
    {
        information = 1;
        HowToB.SetActive(false);
        info.SetActive(true);
        BackB.SetActive(true);
        PB.SetActive(false);
        attemptB.SetActive(false);
        healthB.SetActive(false);
    }
    public void Back()
    {
        information = 0;
        HowToB.SetActive(true);
        info.SetActive(false);
        BackB.SetActive(false);
        PB.SetActive(true);
        attemptB.SetActive(true);
        healthB.SetActive(true);
    }
}
