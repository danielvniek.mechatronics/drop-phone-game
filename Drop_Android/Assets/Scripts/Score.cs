﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Score : MonoBehaviour
{
    Text SC;
    // Start is called before the first frame update
    void Start()
    {
        SC = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        SC.text = "Score: " + BucketGenerator.score;
    }
}
