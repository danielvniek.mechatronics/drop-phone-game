# Description #
This game was built using Unity2D and requires the player to drop balls of the correct colour into buckets of the corect colour and do this fast enough so that their health does not run out. The figure below shows a screenshot from the game. The game works on both Android and Apple.\
<img src = "/doc/pp.JPG" height = "600"> \

 A demo of the video is shown on \
https://www.youtube.com/watch?v=2a73WZmCE8I \

Note that only the Unity assets (minus the music tracks) are in the repo due to size constraints for Git repositories.
